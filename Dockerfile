FROM mkenney/npm:debian
MAINTAINER xfesuac

# Expose port
EXPOSE 1730

# Make ssh dir
RUN mkdir /root/.ssh/

# Copy over private key, and set permissions
ADD id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa

# Create known_hosts
RUN touch /root/.ssh/known_hosts
# Add bitbuckets key
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# Clone - git@notabug.org:xfesuac/remixDeezNutz.git
RUN git clone https://notabug.org/xfesuac/deezNutz.git /deezNutz

# Finish
WORKDIR /deezNutz
RUN ["npm", "install"]
RUN ["npm", "run", "dist:linux"]
WORKDIR /deezNutz/app
CMD ["node", "app.js"]
VOLUME ["/downloads"]